global exit
global panic
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


section .data
buffer: dq 0

section .text

exit:
    mov rax, 60        
    mov rdi, 0         
    syscall

string_length:
	xor rax, rax 
	.loop:
		cmp byte[rdi+rax], 0 
		je .end 
		inc rax 
		jmp .loop 
	.end:
		ret
panic:
    call string_length 
    mov rdx, rax       
    mov rsi, rdi       
    mov rdi, 2         
    mov rax, 1         
    syscall
    mov rax, 60        
    mov rdi, 1         
    syscall

print_string:
	call string_length 
	mov rsi,rdi 
	mov rdx,rax
	mov rax,1
	mov rdi,1
	syscall
	xor rax,rax
	ret



print_newline:
	mov rdi, 0
print_char:
    xor rax, rax
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret




print_uint:
	push r12
	push r13
	mov r12,rsp 
	mov r13,10 
	mov rax,rdi 
	dec rsp
	mov byte[rsp],0 ; 
	.loop:
		dec rsp 
		xor rdx,rdx 
		div r13 
		add rdx,0x30 
		mov byte[rsp],dl 
		test rax,rax 
		jz .print 
		jmp .loop
	.print:
		mov rdi,rsp
		call print_string ;
		mov rsp,r12 ;
	pop r13 
	pop r12 
	ret

print_int:
	xor rax,rax 
	mov rax,rdi 
	test rax,rax 
	jns .pos 
	mov rdi,'-' 
	push rax
	call print_char 
	pop rax 
	neg rax 
	mov rdi,rax 
	.pos: 
		call print_uint 
		ret
		
string_equals:
	call string_length 
	mov rcx,rax 
	xchg rdi, rsi
	call string_length
	cmp rax,rcx 
	jne .not_equals 
	repe cmpsb
	jne .not_equals 
	mov rax,1 
	ret
	.not_equals:
		mov rax,0 
		ret	

read_char:
	dec rsp 
	mov rax,0 
	mov rdi,0 
	mov rsi,rsp 
	mov rdx,1 
	syscall 
	mov rax, [rsp] 
	inc rsp 
	ret

read_word:
	push rbx 
    	mov r8, rsi 
    	mov r9, rdi
    	xor r10, r10 
    	push rdi 
    	xor rdi, rdi 
    	mov rdx, 1 
    	.next:
    		inc r10 ;
        	xor rax, rax ;
        	mov rsi, r9 ;
        	syscall
    		cmp r10, r8 ;
    		je .end ;
        	cmp byte [r9], 0xca ;
        	je .end ;
        	cmp byte [r9], 0x21 ;
        	jb .next ;
    	.again:
        	dec r8
        	cmp r8, 1
        	je .fail
        	inc r9
        	xor rax, rax
        	mov rsi, r9
        	syscall
        	cmp byte [r9], 0x21
        	jb .end
        	cmp byte [r9], 0xca
        	je .end
        	jmp .again
    	.end:
        	mov byte [r9], 0
        	pop rax
       		mov rdx, r9
        	sub rdx, rax
        	pop rbx
        	ret
    	.fail:
        	pop rax
        	mov rax, 0
        	mov rdx, 0
        	pop rbx
        	ret
			
parse_uint:
	call string_length 
	mov rcx,rax 
	mov rsi,rdi
	xor rdx,rdx
	xor rax,rax 
	.pars: 
		xor rdi,rdi 
		mov dil,byte[rsi+rdx] 
		cmp dil,'0' 
		jb .end 
		cmp dil,'9' 
		ja .end 
		sub dil,'0' 
		imul rax,10 
		add rax,rdi 
		inc rdx 
		dec rcx ;
		jnz .pars 
	.end:
		ret

parse_int:
	cmp byte[rdi],'-' 
	je .minus 
	jmp parse_uint 
	.minus:
		inc rdi 
		call parse_uint
		test rdx,rdx
		jz .null
		neg rax 
		inc rdx
		ret
	.null:
		xor rax,rax
		ret
		
string_copy:
	xor rax, rax
	xor r9, r9 
	xor rcx, rcx 
    	call string_length 
    	push rax 
    	push rsi 
	.loop:
    		cmp rcx, rdx 
		je .error 
    		mov r8, [rdi+rcx] 
    		mov [rsi+rcx], r8 
    		cmp rax, 0 
    		je .end
    		dec rax 
    		inc rcx
    		jmp .loop 
	.end:
		pop rsi 
		pop rax 
    		mov byte [rsi+rax], 0 
    		ret
	.error:
    	pop rsi 
		pop rax 
		mov rax, 0 
		ret

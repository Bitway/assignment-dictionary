%define colon_head 0 
%macro colon 2
%%word_head: dq colon_head     
db %1, 0                       
%2:                            

%define colon_head %%word_head 
%endmacro

colon "head", word_head
db "Hello, world!", 0

colon "test", word_test
db "Test String", 0

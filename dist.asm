global find_word
extern string_equals

section .text
find_word:
  push r12
  push r13
  mov r12, rdi
  mov r13, rsi
  xor rax, rax           
find_word_loop:
  test r13, r13          
  jz find_word_not_found
  mov rdi, r12           
  lea rsi, [r13 + 8]     
  call string_equals
  test rax, rax         
  jnz find_word_found
  mov r13, [r13]         
  jmp find_word_loop
find_word_found:
  mov rax, r13
find_word_not_found:     
  pop r13
  pop r12
  ret
